#include <iostream>
#include <thread>
#include <vector>
#include <chrono>
#include <sstream>
#include <random>



class DiningPhilosophersProblem {

public:

    static bool endKeyPressed;
    static std::default_random_engine generator;

    static void philosopherCycle(int philosopherNumber)
    {

        std::uniform_int_distribution<int> distribution(2,4);

        while(!endKeyPressed)
        {

            std::stringstream stream;
            long timeForAction = distribution(generator);

            stream << "Filozof " << philosopherNumber << " bedzie myslal przez " << timeForAction << " sekund." << std::endl;
            std::cout << stream.str();
            stream.str("");
            std::this_thread::sleep_for(std::chrono::seconds(timeForAction));



            stream << "Filozof " <<  philosopherNumber << " bierze lewy widelec" << std::endl;
            std::cout << stream.str();
            stream.str("");



            stream << "Filozof " <<  philosopherNumber << " bierze prawy widelec" << std::endl;
            std::cout << stream.str();
            stream.str("");



            timeForAction = distribution(generator);
            stream << "Filozof " << philosopherNumber << " bedzie jadl przez " << timeForAction << " sekund. Po czym odlozy widelce" << std::endl;
            std::cout << stream.str();
            stream.str("");
            std::this_thread::sleep_for(std::chrono::seconds(timeForAction));

        }

        std::stringstream stream;
        stream << "Filozof " << philosopherNumber << " konczy dzialanie" << std::endl;
        std::cout << stream.str();

    }


};


std::default_random_engine DiningPhilosophersProblem::generator;
bool DiningPhilosophersProblem::endKeyPressed = false;


int main(int argc, char **argv) {

    int threadsAmount;


    if (argc != 2) {

        std::cout << "Podaj liczbe watkow" << std::endl;
        return 3;
    }
    else {

        std::string str(argv[1]);
        threadsAmount = std::stoi(str);
    }


    if(threadsAmount <= 0) {

        std::cout << "Podales niedodatnia liczbe watkow!" << std::endl;
        return 1;
    }

    if(threadsAmount >= 50) {

        std::cout << "Zbyt duza liczba watkow, podaj liczbe mniejsza niz 50" << std::endl;
        return 2;
    }


    std::vector<std::thread> threads(threadsAmount);
    for(int i = 0; i < threadsAmount; i++) {
        threads[i] = std::thread(DiningPhilosophersProblem::philosopherCycle, i + 1);
    }


    char c = 0;
    while(c != 'q')
    {
        std::cin >> c;

    }
    DiningPhilosophersProblem::endKeyPressed = true;


    for(int i = 0; i < threadsAmount; i++) {
        threads[i].join();
    }

    std::cout << "Koniec dzialania programu";

    return 0;
}
